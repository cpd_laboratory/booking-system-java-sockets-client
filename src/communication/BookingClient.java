package communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Class for client
 */
public class BookingClient {
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    /**
     * Function to start connection
     * @param ip IP
     * @param port PORT
     * @throws IOException
     */
    public void startConnection(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    /**
     * Sends a message through the socket
     * @param msg the message
     * @return the response from the server
     * @throws IOException
     */
    public String sendMessage(String msg) throws IOException {
        out.println(msg);
        String resp = in.readLine();
        return resp;
    }

    /**
     * Closes the connection
     * @throws IOException
     */
    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }

    /**
     * Here we are creating two clients and we alternate clients' commands to check if the changes are brought from the server
     * @param args
     */
    public static void main(String[] args) {
        try {
            BookingClient airBnbClient = new BookingClient();
            BookingClient airBnbClient2 = new BookingClient();
            airBnbClient.startConnection("127.0.0.1", 50);
            airBnbClient2.startConnection("127.0.0.1", 50);
            Scanner standardIn = new Scanner(System.in);
            System.out.println("(Paul Filip) Welcome to my Online Booking System app created in Java using sockets");
            int turn = 0;
            while(true) {
                System.out.println("Please enter a command for User"+ turn + ":");
                String line = standardIn.nextLine();

                String response = (turn == 0 ? airBnbClient.sendMessage(line) : airBnbClient2.sendMessage(line));
                turn = (turn + 1) % 2;
                System.out.println(response);
            }
        } catch (Exception e) {
            System.out.println("We are sorry, the connection cannot be established");
        }
    }
}
